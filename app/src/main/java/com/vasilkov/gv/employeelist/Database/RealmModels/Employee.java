package com.vasilkov.gv.employeelist.Database.RealmModels;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Employee extends RealmObject {
    public static final String F_NAME = "f_name";
    public static final String L_NAME = "l_name";
    public static final String BIRTHDAY = "birthday";
    public static final String AVATR_URL = "avatr_url";
    public static final String SPECIALTY = "specialty";
    public static final String ID = "id";

    @PrimaryKey
    private int id;
    private String f_name;
    private String l_name;
    private String birthday;
    private String avatr_url;
    private RealmList<Specialty> specialty;

    public Employee() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getF_name() {
        return this.f_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getL_name() {
        return this.l_name;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBirthday() {
        return this.birthday;
    }

    public void setAvatr_url(String avatr_url) {
        this.avatr_url = avatr_url;
    }

    public String getAvatr_url() {
        return this.avatr_url;
    }

    public void setSpecialty(RealmList<Specialty> specialty) {
        this.specialty = specialty;
    }

    public RealmList<Specialty> getSpecialty() {
        return this.specialty;
    }
}

