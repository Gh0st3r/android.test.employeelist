package com.vasilkov.gv.employeelist.Main.Fragments.EmployeeList;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.vasilkov.gv.employeelist.API.EmployeeListApiImpl;
import com.vasilkov.gv.employeelist.Database.DataBase;
import com.vasilkov.gv.employeelist.Database.Immutable.ImmutableEmployee;

import hugo.weaving.DebugLog;
import rx.Subscription;

/**
 * Created by Gennady on 15.02.2016.
 */
public class EmployeeListFragmentPresenter
        extends MvpBasePresenter<EmployeeListFragmentView> {

    private DataBase dataBase;
    private EmployeeListApiImpl employeeListApi;
    private Subscription subscriptionOnEmployee;
    private int specialtyId;

    public void onCreate() {
        dataBase = DataBase.getInstance();
        employeeListApi = EmployeeListApiImpl.getInstance();
        if (isViewAttached()) {
            getView().setParentActivityInterface();
            getView().setAdapter();
            specialtyId = getView().getSpecialtyId();
            getView().loadData(false);
        }
    }

    @DebugLog
    public void loadData() {
        if (isViewAttached())
            subscriptionOnEmployee = dataBase.getEmployeeListBySpecialtyId(specialtyId)
                    .map(ImmutableEmployee::new)
                    .subscribe(this::getEmployeeFromDatabaseOnNext, this::getEmployeeFromDatabaseOnError);
    }

    @DebugLog
    private void getEmployeeFromDatabaseOnError(Throwable e) {
    }

    @DebugLog
    private void getEmployeeFromDatabaseOnNext(ImmutableEmployee employee) {
        if (isViewAttached()) {
            getView().setData(employee);
            getView().showContent();
        }
    }

    public void onDestroy() {
        if (subscriptionOnEmployee != null) {
            subscriptionOnEmployee.unsubscribe();
        }
    }
}
