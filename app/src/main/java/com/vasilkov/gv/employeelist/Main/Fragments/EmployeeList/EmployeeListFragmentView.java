package com.vasilkov.gv.employeelist.Main.Fragments.EmployeeList;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;
import com.vasilkov.gv.employeelist.Database.Immutable.ImmutableEmployee;

/**
 * Created by Gennady on 15.02.2016.
 */
public interface EmployeeListFragmentView extends MvpLceView<ImmutableEmployee> {
    void setAdapter();
    int getSpecialtyId();
    void setParentActivityInterface();
}
