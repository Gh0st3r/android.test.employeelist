package com.vasilkov.gv.employeelist.Main.Fragments.SpecialtyList;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;
import com.vasilkov.gv.employeelist.Database.Immutable.ImmutableSpecialty;

import java.util.ArrayList;

/**
 * Created by Gennady on 15.02.2016.
 */
public interface SpecialtyListFragmentView extends MvpLceView<ArrayList<ImmutableSpecialty>> {
    void setAdapter();
    void setParentActivityInterface();
}
