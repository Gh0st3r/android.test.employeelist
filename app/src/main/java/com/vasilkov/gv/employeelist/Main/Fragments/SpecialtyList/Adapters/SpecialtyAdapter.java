package com.vasilkov.gv.employeelist.Main.Fragments.SpecialtyList.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vasilkov.gv.employeelist.Database.Immutable.ImmutableSpecialty;
import com.vasilkov.gv.employeelist.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import hugo.weaving.DebugLog;

/**
 * Created by Gennady on 16.02.2016.
 */
public class SpecialtyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ImmutableSpecialty> items = new ArrayList<>();
    private Context mContext;
    private OnItemClickListener onItemClickListener;

    public SpecialtyAdapter(Context context, OnItemClickListener listener) {
        this.mContext = context;
        this.onItemClickListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.speciality_list_item, parent, false),
                position -> onItemClickListener.onClick(getItemSpecialtyId(position)));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder vh = (ItemViewHolder) holder;
        vh.specialityListItemTitle.setText(items.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private int getItemSpecialtyId(int position) {
        return items.get(position).getSpecialty_id();
    }

    @DebugLog
    public void addItems(ArrayList<ImmutableSpecialty> items) {
        this.items = new ArrayList<>();
        for (ImmutableSpecialty item : items) {
            this.items.add(item);
            notifyItemChanged(items.size() - 1);
        }
    }

    public static class ItemViewHolder
            extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.speciality_list_item_title)
        TextView specialityListItemTitle;

        private ViewHolderItemClickListener listener;

        public ItemViewHolder(View itemView, ViewHolderItemClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.listener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(getAdapterPosition());
        }

        public interface ViewHolderItemClickListener {
            void onClick(int position);
        }
    }

    public interface OnItemClickListener {
        void onClick(int specialtyId);
    }
}
