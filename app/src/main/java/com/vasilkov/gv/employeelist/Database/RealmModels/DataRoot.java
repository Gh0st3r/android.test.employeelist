package com.vasilkov.gv.employeelist.Database.RealmModels;

import io.realm.RealmList;
import io.realm.RealmObject;

public class DataRoot extends RealmObject {
    public static final String RESPONSE = "response";
    public static final int CONSISTENCE = 1;

    private RealmList<Employee> response;

    public DataRoot() {
    }

    public void setResponse(RealmList<Employee> response) {
        this.response = response;
    }

    public RealmList<Employee> getResponse() {
        return this.response;
    }
}

