package com.vasilkov.gv.employeelist.Main.Fragments.EmployeeList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceFragment;
import com.vasilkov.gv.employeelist.Database.Immutable.ImmutableEmployee;
import com.vasilkov.gv.employeelist.Enums.FRAGMENTS;
import com.vasilkov.gv.employeelist.Enums.KEYS;
import com.vasilkov.gv.employeelist.Main.Activity.MainActivityView;
import com.vasilkov.gv.employeelist.Main.Fragments.EmployeeList.Adapters.EmployeeAdapter;
import com.vasilkov.gv.employeelist.R;

import hugo.weaving.DebugLog;

/**
 * Created by Gennady on 15.02.2016.
 */
public class EmployeeListFragment
        extends MvpLceFragment<RecyclerView,
        ImmutableEmployee,
        EmployeeListFragmentView,
        EmployeeListFragmentPresenter>
        implements EmployeeListFragmentView, EmployeeAdapter.OnItemClickListener {

    private final static int SPECIALTY_ID_DEFAULT_VALUE = -1;

    private EmployeeAdapter adapter;
    private Context mContext;
    private MainActivityView parentActivityInterface;

    @DebugLog
    public static EmployeeListFragment newInstance(int specialtyId){
        Bundle bundle = new Bundle();
        bundle.putInt(KEYS.SPECIALTY_ID_KEY.name(), specialtyId);
        EmployeeListFragment fragment = new EmployeeListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @DebugLog
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        this.mContext = view.getContext();
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        presenter.onCreate();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (parentActivityInterface != null) {
            parentActivityInterface.showBackButton();
            parentActivityInterface.setTitle(getResources().getString(R.string.employee_list_title));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_employee_list, null);
    }

    @DebugLog
    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return e.getMessage();
    }

    @NonNull
    @Override
    public EmployeeListFragmentPresenter createPresenter() {
        return new EmployeeListFragmentPresenter();
    }

    @DebugLog
    @Override
    public void setData(ImmutableEmployee data) {
        adapter.addItem(data);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.loadData();
    }

    @Override
    public void setAdapter() {
        adapter = new EmployeeAdapter(mContext, this);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        contentView.setLayoutManager(manager);
        contentView.setItemAnimator(new DefaultItemAnimator());
        contentView.setAdapter(adapter);
    }

    @Override
    public int getSpecialtyId() {
        return getArguments().getInt(KEYS.SPECIALTY_ID_KEY.name(), SPECIALTY_ID_DEFAULT_VALUE);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.onDestroy();
    }

    @Override
    public void onClick(int employeeId) {
        Bundle bundle = new Bundle();
        bundle.putInt(KEYS.EMPLOYEE_ID_KEY.name(), employeeId);
        parentActivityInterface.requestCommitFragment(FRAGMENTS.EMPLOYEE_INFO, bundle);
    }

    @Override
    public void setParentActivityInterface() {
        if (getActivity() instanceof MainActivityView)
            parentActivityInterface = (MainActivityView) getActivity();
        else
            throw new ClassCastException(
                    mContext.getString(R.string.exception_class_cast_mainactivityview));
    }
}
