package com.vasilkov.gv.employeelist.Database.Immutable;

import java.util.List;

public class ImmutableDataRoot {
    public static final String RESPONSE = "response";
    public static final int CONSISTENCE = 1;

    private List<ImmutableEmployee> response;

    public ImmutableDataRoot() {
    }

    public void setResponse(List<ImmutableEmployee> response) {
        this.response = response;
    }

    public List<ImmutableEmployee> getResponse() {
        return this.response;
    }
}

