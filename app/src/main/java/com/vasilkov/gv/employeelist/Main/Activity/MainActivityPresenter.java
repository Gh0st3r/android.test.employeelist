package com.vasilkov.gv.employeelist.Main.Activity;

import android.os.Bundle;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.vasilkov.gv.employeelist.Database.DataBase;
import com.vasilkov.gv.employeelist.Enums.FRAGMENTS;
import com.vasilkov.gv.employeelist.Enums.KEYS;
import com.vasilkov.gv.employeelist.Main.Fragments.EmployeeInfo.EmployeeInfoFragment;
import com.vasilkov.gv.employeelist.Main.Fragments.EmployeeList.EmployeeListFragment;
import com.vasilkov.gv.employeelist.Main.Fragments.SpecialtyList.SpecialtyListFragment;

/**
 * Created by Gennady on 15.02.2016.
 */
public class MainActivityPresenter extends MvpBasePresenter<MainActivityView> {

    public void onCreate() {
        if (isViewAttached())
            getView().setToolbar();
        commitFragment(FRAGMENTS.SPECIALITY_LIST, null);
    }

    public void commitFragment(FRAGMENTS fragmentName, Bundle bundle) {
        if (isViewAttached()) {
            if (!getView().isFragmentExist(fragmentName))
                switch (fragmentName) {
                    case SPECIALITY_LIST:
                        SpecialtyListFragment specialtyListFragment = SpecialtyListFragment.newInstance();
                        getView().commitFragment(specialtyListFragment, fragmentName);
                        break;
                    case EMPLOYEE_LIST:
                        EmployeeListFragment employeeListFragment =
                                EmployeeListFragment.newInstance(
                                        bundle.getInt(KEYS.SPECIALTY_ID_KEY.name()));
                        getView().commitFragment(employeeListFragment, fragmentName);
                        break;
                    case EMPLOYEE_INFO:
                        EmployeeInfoFragment employeeInfoFragment =
                                EmployeeInfoFragment.newInstance(
                                        bundle.getInt(KEYS.EMPLOYEE_ID_KEY.name()));
                        getView().commitFragment(employeeInfoFragment, fragmentName);
                        break;
                }

        }
    }

    public void onDestroy() {
        DataBase.getInstance().closeDatabase();
    }
}
