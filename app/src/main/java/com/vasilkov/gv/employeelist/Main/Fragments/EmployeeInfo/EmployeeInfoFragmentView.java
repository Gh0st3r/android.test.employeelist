package com.vasilkov.gv.employeelist.Main.Fragments.EmployeeInfo;

import android.content.Context;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by Gennady on 17.02.2016.
 */
public interface EmployeeInfoFragmentView extends MvpView {
    int getEmployeeId();
    void setEmployeeInfoName(String name);
    void setEmployeeInfoAge(String age);
    void setInfoAgeVisible(boolean isVisible);
    void setEmployeeInfoBirthdate(String birthdate);
    void setEmployeeInfoSpecialty(String specialty);
    void setEmployeeInfoAvatar(String url);
    Context getContext();
}
