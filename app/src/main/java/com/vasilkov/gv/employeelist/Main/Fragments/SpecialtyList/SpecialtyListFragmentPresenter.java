package com.vasilkov.gv.employeelist.Main.Fragments.SpecialtyList;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.vasilkov.gv.employeelist.API.EmployeeListApiImpl;
import com.vasilkov.gv.employeelist.Database.DataBase;
import com.vasilkov.gv.employeelist.Database.Immutable.ImmutableSpecialty;
import com.vasilkov.gv.employeelist.Database.RealmModels.Specialty;

import java.util.ArrayList;

import hugo.weaving.DebugLog;
import io.realm.RealmResults;
import retrofit.client.Response;
import rx.Subscription;

/**
 * Created by Gennady on 15.02.2016.
 */
public class SpecialtyListFragmentPresenter
        extends MvpBasePresenter<SpecialtyListFragmentView> {

    private DataBase dataBase;
    private EmployeeListApiImpl employeeListApi;
    private Subscription subscriptionOnSpecialty;
    private Subscription apiSpecSubscription;

    public void onCreate() {
        dataBase = DataBase.getInstance();
        employeeListApi = EmployeeListApiImpl.getInstance();
        if (isViewAttached()) {
            getView().setParentActivityInterface();
            getView().setAdapter();
            getView().loadData(false);
        }
    }

    @DebugLog
    public void loadData() {
        if (isViewAttached())
            getView().showLoading(false);
        apiSpecSubscription = employeeListApi.getEmployeeList()
                .subscribe(this::getSpecFromApiOnNext, this::getSpecFromApiOnError);
        subscriptionOnSpecialty = dataBase.getSpecialtyList()
                .subscribe(this::getSpecFromDatabaseOnNext, this::getSpecFromDatabaseOnError);
    }

    @DebugLog
    private void getSpecFromDatabaseOnError(Throwable e){

    }

    @DebugLog
    private void getSpecFromDatabaseOnNext(RealmResults<Specialty> specialties){
        if (isViewAttached()) {
            if (specialties.size() > 0) {
                ArrayList<ImmutableSpecialty> list = new ArrayList<>(specialties.size());
                for (Specialty item : specialties)
                    list.add(mapSpec(item));
                getView().setData(list);
                getView().showContent();
            } else {
                Throwable throwable = new Throwable("SOME ERROR\nPRESS FOR REFRESH");
                getView().showError(throwable, false);
            }
        }
    }

    private ImmutableSpecialty mapSpec(Specialty specialty){
        ImmutableSpecialty spec = new ImmutableSpecialty();
        spec.setName(specialty.getName());
        spec.setSpecialty_id(specialty.getSpecialty_id());
        return spec;
    }

    private void getSpecFromApiOnError(Throwable e){

    }

    private void getSpecFromApiOnNext(Response response){
        dataBase.saveEmployeesToDB(response);
    }

    public void onDestroy() {
        if (subscriptionOnSpecialty != null) {
            subscriptionOnSpecialty.unsubscribe();
        }
        if (apiSpecSubscription != null) {
            apiSpecSubscription.unsubscribe();
        }
    }
}
