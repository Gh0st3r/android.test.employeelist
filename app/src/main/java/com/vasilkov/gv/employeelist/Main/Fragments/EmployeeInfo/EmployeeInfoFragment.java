package com.vasilkov.gv.employeelist.Main.Fragments.EmployeeInfo;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.florent37.glidepalette.BitmapPalette;
import com.github.florent37.glidepalette.GlidePalette;
import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.vasilkov.gv.employeelist.Enums.KEYS;
import com.vasilkov.gv.employeelist.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hugo.weaving.DebugLog;

/**
 * Created by Gennady on 17.02.2016.
 */
public class EmployeeInfoFragment
        extends MvpFragment<EmployeeInfoFragmentView, EmployeeInfoFragmentPresenter>
        implements EmployeeInfoFragmentView {

    private final static int EMPLOYEE_ID_DEFAULT_VALUE = -1;
    private final static int ANIM_DURATION_FADE_GLIDE = 400;
    private final static int ANIM_DURATION_FADE_GLIDE_PALETTE = 200;

    private ViewHolder vh;
    private Context mContext;

    @NonNull
    @Override
    public EmployeeInfoFragmentPresenter createPresenter() {
        return new EmployeeInfoFragmentPresenter();
    }

    @DebugLog
    public static EmployeeInfoFragment newInstance(int employeeId) {
        Bundle bundle = new Bundle();
        bundle.putInt(KEYS.EMPLOYEE_ID_KEY.name(), employeeId);
        EmployeeInfoFragment fragment = new EmployeeInfoFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @DebugLog
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mContext = view.getContext();
        createViewHolder(view);
        presenter.onCreate();
    }

    @Override
    public void onStart() {
        super.onStart();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

    private void createViewHolder(View view) {
        vh = new ViewHolder(view);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_employee_info, null);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public int getEmployeeId() {
        return getArguments().getInt(KEYS.EMPLOYEE_ID_KEY.name(), EMPLOYEE_ID_DEFAULT_VALUE);
    }

    @Override
    public void setEmployeeInfoName(String name) {
        vh.employeeInfoName.setText(name);
    }

    @Override
    public void setEmployeeInfoAge(String age) {
        vh.employeeInfoAge.setText(age);
    }

    @Override
    public void setEmployeeInfoBirthdate(String birthdate) {
        vh.employeeInfoBirthdate.setText(birthdate);
    }

    @Override
    public void setInfoAgeVisible(boolean isVisible) {
        if (isVisible) vh.employeeInfoAge.setVisibility(View.VISIBLE);
        else vh.employeeInfoAge.setVisibility(View.GONE);
    }

    @Override
    public Context getContext() {
        return this.mContext;
    }

    @Override
    public void setEmployeeInfoSpecialty(String specialty) {
        vh.employeeInfoSpecialty.setText(specialty);
    }

    @Override
    public void setEmployeeInfoAvatar(String url) {
        Glide.with(mContext)
                .load(url)
                .listener(GlidePalette.with(url)
                        .use(GlidePalette.Profile.MUTED)
                        .intoBackground(vh.employeeInfoAvatar)
                        .crossfade(true, ANIM_DURATION_FADE_GLIDE_PALETTE))
                .crossFade(ANIM_DURATION_FADE_GLIDE)
                .error(R.color.black)
                .into(vh.employeeInfoAvatar);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.onDestroy();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.fragment_info_back_btn)
    public void onClick() {
        getActivity().onBackPressed();
    }

    static class ViewHolder {
        @Bind(R.id.employee_info_avatar)
        ImageView employeeInfoAvatar;
        @Bind(R.id.employee_info_name)
        TextView employeeInfoName;
        @Bind(R.id.employee_info_specialty)
        TextView employeeInfoSpecialty;
        @Bind(R.id.employee_info_birthdate)
        TextView employeeInfoBirthdate;
        @Bind(R.id.employee_info_age)
        TextView employeeInfoAge;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
