package com.vasilkov.gv.employeelist;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Gennady on 16.02.2016.
 */
public class EmployeeListApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        setRealm();
    }

    private void setRealm(){
        RealmConfiguration configuration =
                new RealmConfiguration.Builder(getApplicationContext()).build();
        Realm.setDefaultConfiguration(configuration);
    }
}
