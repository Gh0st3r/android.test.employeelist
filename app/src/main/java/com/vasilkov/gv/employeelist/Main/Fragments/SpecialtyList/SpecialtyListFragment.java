package com.vasilkov.gv.employeelist.Main.Fragments.SpecialtyList;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceFragment;
import com.vasilkov.gv.employeelist.Database.Immutable.ImmutableSpecialty;
import com.vasilkov.gv.employeelist.Enums.FRAGMENTS;
import com.vasilkov.gv.employeelist.Enums.KEYS;
import com.vasilkov.gv.employeelist.Main.Activity.MainActivityView;
import com.vasilkov.gv.employeelist.Main.Fragments.SpecialtyList.Adapters.SpecialtyAdapter;
import com.vasilkov.gv.employeelist.R;

import java.util.ArrayList;

import hugo.weaving.DebugLog;

/**
 * Created by Gennady on 15.02.2016.
 */
public class SpecialtyListFragment
        extends MvpLceFragment<RecyclerView,
        ArrayList<ImmutableSpecialty>,
        SpecialtyListFragmentView,
        SpecialtyListFragmentPresenter>
        implements SpecialtyListFragmentView, SpecialtyAdapter.OnItemClickListener {

    private SpecialtyAdapter adapter;
    private Context mContext;
    private MainActivityView parentActivityInterface;

    @DebugLog
    public static SpecialtyListFragment newInstance() {
        return new SpecialtyListFragment();
    }

    @DebugLog
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        this.mContext = view.getContext();
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        presenter.onCreate();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (parentActivityInterface != null) {
            parentActivityInterface.hideBackButton();
            parentActivityInterface.setTitle(getResources().getString(R.string.employee_specialty_list_title));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_speciality_list, null);
    }

    @DebugLog
    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return e.getMessage();
    }

    @NonNull
    @Override
    public SpecialtyListFragmentPresenter createPresenter() {
        return new SpecialtyListFragmentPresenter();
    }

    @DebugLog
    @Override
    public void setData(ArrayList<ImmutableSpecialty> data) {
        adapter.addItems(data);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.loadData();
    }

    @Override
    public void setAdapter() {
        adapter = new SpecialtyAdapter(mContext, this);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        contentView.setLayoutManager(manager);
        contentView.setItemAnimator(new DefaultItemAnimator());
        contentView.setAdapter(adapter);
    }

    @Override
     public void setParentActivityInterface() {
        if (getActivity() instanceof MainActivityView)
            parentActivityInterface = (MainActivityView) getActivity();
        else
            throw new ClassCastException(
                    mContext.getString(R.string.exception_class_cast_mainactivityview));
    }

    @DebugLog
    @Override
    public void onClick(int specialtyId) {
        Bundle bundle = new Bundle();
        bundle.putInt(KEYS.SPECIALTY_ID_KEY.name(), specialtyId);
        parentActivityInterface.requestCommitFragment(FRAGMENTS.EMPLOYEE_LIST, bundle);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.onDestroy();
    }
}
