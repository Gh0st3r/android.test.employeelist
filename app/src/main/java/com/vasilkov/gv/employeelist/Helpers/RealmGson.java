package com.vasilkov.gv.employeelist.Helpers;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;

import io.realm.RealmObject;

/**
 * Created by Gennady on 17.02.2016.
 */
public class RealmGson {

    public RealmGson() {
    }

    public Gson getRealmGson(){
        return new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .excludeFieldsWithModifiers(Modifier.FINAL)
                .setDateFormat("yyyy-MM-dd")
                .create();
    }
}
