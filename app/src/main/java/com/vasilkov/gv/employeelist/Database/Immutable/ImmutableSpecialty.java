package com.vasilkov.gv.employeelist.Database.Immutable;

public class ImmutableSpecialty {
    public static final String SPECIALTY_ID = "specialty_id";
    public static final String NAME = "name";

    private int specialty_id;
    private String name;

    public ImmutableSpecialty() {
    }

    public void setSpecialty_id(int specialty_id) {
        this.specialty_id = specialty_id;
    }

    public int getSpecialty_id() {
        return this.specialty_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}

