package com.vasilkov.gv.employeelist.Main.Fragments.EmployeeInfo;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.vasilkov.gv.employeelist.Database.DataBase;
import com.vasilkov.gv.employeelist.Database.Immutable.ImmutableEmployee;
import com.vasilkov.gv.employeelist.Database.Immutable.ImmutableSpecialty;
import com.vasilkov.gv.employeelist.Helpers.CalendarUtils;
import com.vasilkov.gv.employeelist.R;

import io.realm.RealmObject;
import rx.Subscription;

/**
 * Created by Gennady on 17.02.2016.
 */
public class EmployeeInfoFragmentPresenter extends MvpBasePresenter<EmployeeInfoFragmentView> {

    private DataBase dataBase;
    private int employeeId;
    private Subscription employeeSubscription;

    public void onCreate() {
        dataBase = DataBase.getInstance();
        if (isViewAttached())
            employeeId = getView().getEmployeeId();
        initInfo();
    }

    public void initInfo() {
        employeeSubscription = dataBase.getEmployeeById(employeeId)
                .filter(RealmObject::isLoaded)
                .map(ImmutableEmployee::new)
                .subscribe(this::onNextGetEmployeeFromDB);
    }

    private void onNextGetEmployeeFromDB(ImmutableEmployee employee) {
        if (isViewAttached()) {
            String specStr = "";
            String div = ", ";
            for (int i = 0; i < employee.getSpecialty().size(); i++ ){
                ImmutableSpecialty specialty = employee.getSpecialty().get(i);
                if (i == 0)
                specStr += specialty.getName();
                else specStr += (div + specialty.getName());
            }
            getView().setEmployeeInfoSpecialty(specStr);
            getView().setEmployeeInfoName(
                    String.format("%s %s", employee.getL_name(), employee.getF_name()));
            if (employee.getBirthday() != null && !employee.getBirthday().equals("")) {
                getView().setEmployeeInfoBirthdate(
                        String.format("%s",
                                CalendarUtils.formattedDate(employee.getBirthday())));
                getView().setEmployeeInfoAge(
                        String.format("%s %s",
                                CalendarUtils.calculateAge(employee.getBirthday()).toString(),
                                getView().getContext().getString(R.string.employee_info_age)));
                getView().setInfoAgeVisible(true);
            } else {
                getView().setEmployeeInfoBirthdate(" - ");
                getView().setInfoAgeVisible(false);
            }

            getView().setEmployeeInfoAvatar(employee.getAvatr_url());
        }
    }

    public void onDestroy() {
        if (employeeSubscription != null) {
            employeeSubscription.unsubscribe();
        }
    }
}
