package com.vasilkov.gv.employeelist.Database.RealmModels;

import io.realm.RealmObject;
import io.realm.annotations.Index;

public class Specialty extends RealmObject {
    public static final String SPECIALTY_ID = "specialty_id";
    public static final String NAME = "name";

    @Index
    private int specialty_id;
    private String name;

    public Specialty() {
    }

    public void setSpecialty_id(int specialty_id) {
        this.specialty_id = specialty_id;
    }

    public int getSpecialty_id() {
        return this.specialty_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}

