package com.vasilkov.gv.employeelist.Database;

import com.vasilkov.gv.employeelist.Database.RealmModels.DataRoot;
import com.vasilkov.gv.employeelist.Database.RealmModels.Employee;
import com.vasilkov.gv.employeelist.Database.RealmModels.Specialty;
import com.vasilkov.gv.employeelist.Helpers.RealmGson;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import rx.Observable;

/**
 * Created by Gennady on 15.02.2016.
 */
public class DataBase {

    private static volatile DataBase instance;
    private Realm realm;

    private DataBase() {
        realm = Realm.getDefaultInstance();
    }

    public static DataBase getInstance() {
        DataBase localInstance = instance;
        if (localInstance == null) {
            synchronized (DataBase.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new DataBase();
                }
            }
        }
        return localInstance;
    }

    public Observable<RealmResults<Specialty>> getSpecialtyList(){
        return realm.distinctAsync(Specialty.class, Specialty.SPECIALTY_ID)
                .asObservable();
    }

    public Observable<Employee> getEmployeeListBySpecialtyId(int specialtyId){
        return realm.where(Employee.class)
                .findAllSortedAsync(Employee.L_NAME, Sort.ASCENDING)
                .asObservable()
                .filter(RealmResults::isLoaded)
                .flatMap(Observable::from)
                .filter(employee -> employee.getSpecialty().where()
                        .equalTo(Specialty.SPECIALTY_ID, specialtyId).findAll().size() > 0);
    }

    public Observable<Employee> getEmployeeById(int id){
        return realm.where(Employee.class)
                .equalTo(Employee.ID, id)
                .findFirstAsync()
                .asObservable();
    }

    public void saveEmployeesToDB(Response response){
        DataRoot data = new RealmGson()
                .getRealmGson().fromJson(getStringFromResponse(response), DataRoot.class);

        realm.executeTransaction(realm1 -> {
            for (int i = 0; i < data.getResponse().size(); i++) {
                Employee employee = data.getResponse().get(i);
                employee.setId(i);
                realm1.copyToRealmOrUpdate(employee);
            }
        });
    }

    private String getStringFromResponse(Response response) {
        return new String(((TypedByteArray) response.getBody()).getBytes());
    }

    private void clearDB(){
        realm.executeTransaction(realm1 -> {
            realm1.clear(Specialty.class);
            realm1.clear(Employee.class);
            realm1.clear(DataRoot.class);
        });
    }

    public void closeDatabase(){
        this.realm.close();
        instance = null;
    }


}
