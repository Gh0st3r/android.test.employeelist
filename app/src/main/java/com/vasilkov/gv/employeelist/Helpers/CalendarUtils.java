package com.vasilkov.gv.employeelist.Helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import hugo.weaving.DebugLog;

/**
 * Created by Gennady on 17.02.2016.
 */
public class CalendarUtils {

    private static Date date = new Date();

    private enum NUMBER_PATTERN_FOR_USE {
        ONE, TWO
    }

    @DebugLog
    public static Integer calculateAge(final String birthday)
    {
        date = new Date();

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(getDateByString(birthday, NUMBER_PATTERN_FOR_USE.ONE));
        date = dob.getTime();
        // include day of birth
        dob.add(Calendar.DAY_OF_MONTH, -1);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        if (age > 200){
            dob.setTime(getDateByString(birthday, NUMBER_PATTERN_FOR_USE.TWO));
            date = dob.getTime();
            // include day of birth
            dob.add(Calendar.DAY_OF_MONTH, -1);

            age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
            if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR)) {
                age--;
            }
        }


        return age;
    }

    @DebugLog
    public static Date getDateByString(final String dateString, NUMBER_PATTERN_FOR_USE patternForUse){
        SimpleDateFormat sdf = new SimpleDateFormat();
        switch (patternForUse){
            case ONE:
                sdf.applyPattern("yyyy-MM-dd");
                break;
            case TWO:
                sdf.applyPattern("dd-MM-yyyy");

        }
        Date cal = null;
        try {
            cal = sdf.parse(dateString);

        } catch (ParseException e) {

            e.printStackTrace();
        }

        return cal;
    }

    @DebugLog
    public static String formattedDate(String dateString){
        calculateAge(dateString);
        SimpleDateFormat sdfOut = new SimpleDateFormat("dd.MM.yyyy");
        return sdfOut.format(date);
    }
}
