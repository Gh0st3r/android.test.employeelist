package com.vasilkov.gv.employeelist.Enums;

/**
 * Created by Gennady on 15.02.2016.
 */
public enum FRAGMENTS {
    SPECIALITY_LIST, EMPLOYEE_LIST, EMPLOYEE_INFO
}
