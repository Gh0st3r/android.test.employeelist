package com.vasilkov.gv.employeelist.Main.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.hannesdorfmann.mosby.mvp.MvpView;
import com.vasilkov.gv.employeelist.Enums.FRAGMENTS;

/**
 * Created by Gennady on 15.02.2016.
 */
public interface MainActivityView extends MvpView {
    void requestCommitFragment(FRAGMENTS fragmentName, Bundle bundle);
    void commitFragment(Fragment fragment, FRAGMENTS fragmentName);
    boolean isFragmentExist(FRAGMENTS fragmentName);
    void setToolbar();
    void hideBackButton();
    void showBackButton();
    void setTitle(String title);
}
