package com.vasilkov.gv.employeelist.API;

import com.vasilkov.gv.employeelist.BuildConfig;
import com.vasilkov.gv.employeelist.Helpers.RealmGson;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Gennady on 15.02.2016.
 */
public class EmployeeListApiImpl implements EmployeeListApi {

    private final int DELAY_TIME = 1;
    private final int TIMEOUT_TIME = 4;

    private EmployeeListApiImpl() {
    }

    private static volatile EmployeeListApiImpl instance;

    public static EmployeeListApiImpl getInstance() {
        EmployeeListApiImpl localInstance = instance;
        if (localInstance == null) {
            synchronized (EmployeeListApiImpl.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new EmployeeListApiImpl();
                }
            }
        }
        return localInstance;
    }

    private EmployeeListApi api() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(new RealmGson().getRealmGson()))
                .setEndpoint(BuildConfig.HOST)
                .build();
        return restAdapter.create(EmployeeListApi.class);
    }

    @Override
    public Observable<Response> getEmployeeList() {
        return api().getEmployeeList()
                .retryWhen(observable -> observable.delay(DELAY_TIME, TimeUnit.SECONDS))
                .timeout(TIMEOUT_TIME, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
