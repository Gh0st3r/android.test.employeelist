package com.vasilkov.gv.employeelist.API;

import retrofit.client.Response;
import retrofit.http.GET;
import rx.Observable;

/**
 * Created by Gennady on 15.02.2016.
 */
public interface EmployeeListApi {

    @GET("/images/testTask.json")
    Observable<Response> getEmployeeList();
}
