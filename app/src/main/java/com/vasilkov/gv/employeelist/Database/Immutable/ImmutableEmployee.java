package com.vasilkov.gv.employeelist.Database.Immutable;

import com.vasilkov.gv.employeelist.Database.RealmModels.Employee;
import com.vasilkov.gv.employeelist.Database.RealmModels.Specialty;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

public class ImmutableEmployee {
    public static final String F_NAME = "f_name";
    public static final String L_NAME = "l_name";
    public static final String BIRTHDAY = "birthday";
    public static final String AVATR_URL = "avatr_url";
    public static final String SPECIALTY = "specialty";
    public static final String ID = "id";

    private int id;
    private String f_name;
    private String l_name;
    private String birthday;
    private String avatr_url;
    private List<ImmutableSpecialty> specialty;

    public ImmutableEmployee() {
    }

    public ImmutableEmployee(Employee employee) {
        this.id = employee.getId();
        this.f_name = employee.getF_name().toLowerCase();
        this.f_name = Character.toUpperCase(f_name.charAt(0)) + f_name.substring(1);
        this.l_name = employee.getL_name().toLowerCase();
        this.l_name = Character.toUpperCase(l_name.charAt(0)) + l_name.substring(1);
        this.birthday = employee.getBirthday();
        this.avatr_url = employee.getAvatr_url();
        this.specialty = getImmutableSpecList(employee.getSpecialty());
    }

    private List<ImmutableSpecialty> getImmutableSpecList(RealmList<Specialty> list){
        List<ImmutableSpecialty> immutableSpecialties = new ArrayList<ImmutableSpecialty>();
        for (Specialty spec : list){
            immutableSpecialties.add(convertSpecToImmutable(spec));
        }
        return immutableSpecialties;
    }

    private ImmutableSpecialty convertSpecToImmutable(Specialty spec){
        ImmutableSpecialty immutableSpecialty = new ImmutableSpecialty();
        immutableSpecialty.setName(spec.getName());
        immutableSpecialty.setSpecialty_id(spec.getSpecialty_id());
        return immutableSpecialty;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getF_name() {
        return this.f_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getL_name() {
        return this.l_name;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBirthday() {
        return this.birthday;
    }

    public void setAvatr_url(String avatr_url) {
        this.avatr_url = avatr_url;
    }

    public String getAvatr_url() {
        return this.avatr_url;
    }

    public void setSpecialty(List<ImmutableSpecialty> specialty) {
        this.specialty = specialty;
    }

    public List<ImmutableSpecialty> getSpecialty() {
        return this.specialty;
    }
}

