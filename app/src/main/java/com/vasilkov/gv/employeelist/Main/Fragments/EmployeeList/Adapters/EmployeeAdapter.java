package com.vasilkov.gv.employeelist.Main.Fragments.EmployeeList.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vasilkov.gv.employeelist.Database.Immutable.ImmutableEmployee;
import com.vasilkov.gv.employeelist.Helpers.CalendarUtils;
import com.vasilkov.gv.employeelist.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import hugo.weaving.DebugLog;

/**
 * Created by Gennady on 16.02.2016.
 */
public class EmployeeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ImmutableEmployee> items = new ArrayList<>();
    private Context mContext;
    private OnItemClickListener onItemClickListener;


    public EmployeeAdapter(Context context, OnItemClickListener listener) {
        this.mContext = context;
        this.onItemClickListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.employee_list_item, parent, false),
                position -> onItemClickListener.onClick(getItemEmployeeId(position)));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder vh = (ItemViewHolder) holder;
        vh.employeeItemName.setText(String.format("%s %s", items.get(position).getL_name(),
                items.get(position).getF_name()));
        if (items.get(position).getBirthday() != null
                && !items.get(position).getBirthday().isEmpty())
            vh.employeeItemAge.setText(String.format("%s %s",CalendarUtils.
                    calculateAge(items.get(position).getBirthday()).toString(),
                    mContext.getString(R.string.employee_info_age)));
        else vh.employeeItemAge.setText( " - ");

        Glide.with(mContext)
                .load(items.get(position).getAvatr_url())
                .crossFade()
                .error(R.color.black)
                .into(vh.employeeItemAvatar);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @DebugLog
    public void addAllItems(ArrayList<ImmutableEmployee> items) {
        this.items = new ArrayList<>();
        for (ImmutableEmployee item : items) {
            this.items.add(item);
            notifyItemChanged(items.size() - 1);
        }
    }

    @DebugLog
    public void addItem(ImmutableEmployee item) {
        this.items.add(item);
        notifyItemChanged(this.items.size() - 1);
    }

    private int getItemEmployeeId(int position) {
        return items.get(position).getId();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        @Bind(R.id.employee_item_avatar)
        ImageView employeeItemAvatar;
        @Bind(R.id.employee_item_last_name)
        TextView employeeItemName;
//        @Bind(R.id.employee_item_first_name)
//        TextView employeeItemFirstName;
        @Bind(R.id.employee_item_age)
        TextView employeeItemAge;

        private ViewHolderItemClickListener listener;

        public ItemViewHolder(View itemView, ViewHolderItemClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.listener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(getAdapterPosition());
        }

        public interface ViewHolderItemClickListener {
            void onClick(int position);
        }
    }

    public interface OnItemClickListener {
        void onClick(int employeeId);
    }
}
